﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConfirmPreferences : MonoBehaviour {
    private static ConfirmPreferences instance;

    public GameObject confirmBox;
    public string levelName;
    private Dictionary<string, string> storePreferences;

    void Start()
    {
        instance = this;
        storePreferences = new Dictionary<string, string>();

        confirmBox.SetActive(false);

        BackButton backButton = FindObjectOfType<BackButton>();
        if (backButton != null)
            Destroy(backButton.gameObject);
    }

    public static ConfirmPreferences GetInstance()
    {
        return instance;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (storePreferences.Count > 0)
            {
                confirmBox.SetActive(true);
            }
            else {
                LoadScene();
            }
        }
    }

    public void AddPreference(string key, string value)
    {
        storePreferences[key] = value;
    }

    public void SavePreferences()
    {
        foreach (string key in storePreferences.Keys)
        {
            string value = storePreferences[key];
            PlayerPrefs.SetString(key, value);
        }
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(levelName);
    }

    public void SavePreferencesAndLoadScene()
    {
        SavePreferences();
        LoadScene();
    }
}
