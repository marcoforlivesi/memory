﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class OnLoadCreateTile : LoadLevel {
    public int pairNumber;
    public Sillabes.Type[] types;

    override public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Board.GetInstance().CreateTiles(pairNumber, types);
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
