﻿using DG.Tweening;

using System.Collections;
using System.Collections.Generic;

using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class MemoryTile : MonoBehaviour {
    
    public enum State { Covered, Visible, Resolved };
    public State state = State.Covered;
    public Sprite[] alfabet;
    private string value;
    private AudioSource audioSource;

    private Board board;

    // Use this for initialization
    void Start () {
        Toogle(false);
        board = transform.parent.GetComponent<Board>();
        audioSource = transform.GetComponent<AudioSource>();
        string path = "audio/" + name.Length + "/" + name;
        AudioClip clip = Resources.Load<AudioClip>(path);
        audioSource.clip = clip;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPressTile()
    {
        if (!GameMode.GetInstance().IsPlayerTurn())
        {
            return;
        }
        Show();
    }

    public void Show()
    {
        Toogle(true);
        state = State.Visible;
        audioSource.Play();
        GameMode.GetInstance().OnTileSelection(this);
    }

    public void Cover()
    {
        Toogle(false);
        GlobalMessage.GetInstance().Hide();
        state = State.Covered;
    }

    public string GetValue()
    {
        return value;
    }

    public void SetText(string text)
    {
        value = text;
        Transform textChild = transform.GetChild(1);
        Image letterGO = textChild.GetComponentInChildren<Image>();
        int index = text[0] - 'a';
        letterGO.sprite = alfabet[index];
        for (int i = 1; i < text.Length; i++)
        {
            Image letter = Instantiate<Image>(letterGO);
            letter.transform.SetParent(textChild, false);
            index = text[i] - 'a';
            letter.sprite = alfabet[index];
        }
    }

    void Toogle(bool enabled)
    {
        Image image = transform.GetChild(0).GetComponent<Image>();
        image.enabled = enabled;
        transform.GetChild(1).gameObject.SetActive(enabled);
    }

    public bool isSame(MemoryTile other)
    {
        if(value == other.value)
        {
            return true;
        }

        return false;
    }

    public bool isCovered()
    {
        return state == State.Covered; 
    }

    public void Resolved()
    {
        state = State.Resolved;
        Board.GetInstance().RemoveChoice(this);
    }

    override public string ToString()
    {
        return value;
    }
}
