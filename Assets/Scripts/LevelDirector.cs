using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelDirector : MonoBehaviour
{
    static private LevelDirector instance;

    public int[] twoTilesForLevel;
    public int[] threeTilesForLevel;
    public int[] fourTilesForLevel;

    private int index; 

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if(instance == null)
        {
            index = 0;
            SceneManager.sceneLoaded += OnSceneLoaded;
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    virtual public void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (Board.GetInstance() == null || IsCompleted())
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
            instance = null;
            Destroy(gameObject);
            SceneManager.LoadScene("MainMenu");
            return;
        }

        SetLevel();
    }

    static public LevelDirector GetInstance()
    {
        return instance;
    }

    void SetLevel()
    {
        int typeCount = 0;
        int numTiles = 0;
        if (index < twoTilesForLevel.Length && twoTilesForLevel[index] > 0)
        {
            numTiles += twoTilesForLevel[index];
            typeCount++;
        }
        if (index < threeTilesForLevel.Length && threeTilesForLevel[index] > 0)
        {
            numTiles += threeTilesForLevel[index];
            typeCount++;
        }
        if (index < fourTilesForLevel.Length && fourTilesForLevel[index] > 0)
        {
            numTiles += fourTilesForLevel[index];
            typeCount++;
        }

        Sillabes.Type[] types = new Sillabes.Type[typeCount];
        int typeIndex = 0;
        if (twoTilesForLevel[index] > 0)
            types[typeIndex++] = Sillabes.Type.Two;
        if (threeTilesForLevel[index] > 0)
            types[typeIndex++] = Sillabes.Type.Three;
        if (fourTilesForLevel[index] > 0)
            types[typeIndex++] = Sillabes.Type.Four;

        index++;

        Board.GetInstance().CreateTiles(numTiles, types);

    }

    private bool IsCompleted()
    {
        return index >= twoTilesForLevel.Length;
    }
}