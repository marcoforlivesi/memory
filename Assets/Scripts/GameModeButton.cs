﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameModeButton : LoadLevel 
{
	public enum GameModeEnum
	{
		AI, LocalMulti
	};
	public GameModeEnum gameMode;

	override public void Load ()
	{
		base.Load();
	}

    override public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		base.OnSceneLoaded(scene, mode);

		OnLoadCreateTile[] levelButtons = GameObject.FindObjectsOfType<OnLoadCreateTile>();
		
		foreach (OnLoadCreateTile levelButton in levelButtons)
		{
			if (gameMode == GameModeEnum.AI)
			{
				levelButton.levelName = "ModeAI";
			}
            else if (gameMode == GameModeEnum.LocalMulti)
            {
                levelButton.levelName = "ModeLocalMulti";
            }
		}


		SceneManager.sceneLoaded -= OnSceneLoaded;
	}
}
