﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalMessage : MonoBehaviour {
    public Text text;
    private static GlobalMessage instance;

    public static GlobalMessage GetInstance()
    {
        return instance;
    }

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

	public void SetMessage(string message, bool pause = false)
    {
        if (pause)
        {
            Time.timeScale = 0;
        }
        transform.GetChild(0).gameObject.SetActive(true);

        text.text = message;
    }

    void OnLevelWasLoaded(int level)
    {
        Debug.Log("OnLevelWasLoaded");
    }

    public void Hide()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
