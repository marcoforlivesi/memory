﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public Text scoreText;
    private int score;

	// Use this for initialization
	void Start () {
        score = 0;
        scoreText.text = score.ToString();
    }
	
	public void addPoint()
    {
        score++;
        scoreText.text = score.ToString();
    }

    public int GetPoints()
    {
        return score;
    }
}
