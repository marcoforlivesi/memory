﻿//	Copyright (c) 2016 steele of lowkeysoft.com
//        http://lowkeysoft.com
//
//	This software is provided 'as-is', without any express or implied warranty. In
//	no event will the authors be held liable for any damages arising from the use
//	of this software.
//
//	Permission is granted to anyone to use this software for any purpose,
//	including commercial applications, and to alter it and redistribute it freely,
//	subject to the following restrictions:
//
//	1. The origin of this software must not be misrepresented; you must not claim
//	that you wrote the original software. If you use this software in a product,
//	an acknowledgment in the product documentation would be appreciated but is not
//	required.
//
//	2. Altered source versions must be plainly marked as such, and must not be
//	misrepresented as being the original software.
//
//	3. This notice may not be removed or altered from any source distribution.
//
//  =============================================================================
//
// Acquired from https://github.com/steelejay/LowkeySpeech
//
using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Net;

using GoogleSpeech;


public class GoogleVoiceSpeech : BaseVoiceRecognition
{
    const int HEADER_SIZE = 44;

    private int minFreq;
    private int maxFreq;

    private bool micConnected = false;

    private AudioClip audioClip;
    public string apiKey = "AIzaSyDd9VE4JQIHBSzsp68NkaW8HZZrhmd1658";
    private string language = "it";
    private string apiURL;

    static private GoogleVoiceSpeech instance;

    int sampleRate = 16000;

    static public GoogleVoiceSpeech GetInstance()
    {
        if (instance == null)
        {
            instance = new GoogleVoiceSpeech();
        }


        return instance;
    }

    // Use this for initialization
    private GoogleVoiceSpeech()
    {
        ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

        //Check if there is at least one microphone connected
        if (Microphone.devices.Length <= 0)
        {
            //Throw a warning message at the console if there isn't
            Debug.LogWarning("Microphone not connected!");
            return;
        }
        //Set 'micConnected' to true
        micConnected = true;

        //Get the default microphone recording capabilities
        Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

        //According to the documentation, if minFreq and maxFreq are zero, the microphone supports any frequency...
        if (minFreq == 0 && maxFreq == 0)
        {
            //...meaning 44100 Hz can be used as the recording sampling rate
            maxFreq = 44100;
        }

        apiURL = "https://speech.googleapis.com/v1/speech:recognize?key=" + apiKey;


    }

    override public void Start()
    {
        base.Start();
        //If there is a microphone
        if (!micConnected)
        {
            Debug.Log("Microphone not connected!");
            return;
        }

        audioClip = Microphone.Start(null, true, 7, sampleRate); //Currently set for a 7 second clip
    }
    
    override public void OnStop()
    {
        Microphone.End(null); //Stop the audio recording

        //string filePath = SaveFile(audioClip);
        //Debug.Log("Uploading " + filePath);
        
                
        string Response = HttpUploadFile(apiURL, audioClip);
        Debug.Log("Response String: " + Response);

        var jsonresponse = SimpleJSON.JSON.Parse(Response);

        if (jsonresponse != null)
        {
            string resultString = jsonresponse["result"][0].ToString();
            var jsonResults = SimpleJSON.JSON.Parse(resultString);

            SimpleJSON.JSONNode alternative = jsonResults["alternative"];
            if (alternative == null || alternative.Count == 0) return;
            string transcripts = alternative["transcript"].ToString();

            Debug.Log("transcript string: " + transcripts);
            onTextFound(transcripts);

        }

        //File.Delete(filePath); //Delete the Temporary Wav file
    }

    private string SaveFile(AudioClip audioClip)
    {
        float filenameRand = UnityEngine.Random.Range(0.0f, 10.0f);
        string filename = "testing" + filenameRand;

        if (!filename.ToLower().EndsWith(".wav"))
        {
            filename += ".wav";
        }

        var filePath = Path.Combine("testing/", filename);
        filePath = Path.Combine(Application.persistentDataPath, filePath);
        Debug.Log("Created filepath string: " + filePath);

        // Make sure directory exists if user is saving to sub dir.
        Directory.CreateDirectory(Path.GetDirectoryName(filePath));
        SavWav.Save(filePath, audioClip); //Save a temporary Wav File
        Debug.Log("Saving @ " + filePath);

        return filePath;
    }

    public string HttpUploadFile(string url, AudioClip audioClip)
    {
        int maxResults = 3;

        Stream wav = SoundTools.ConvertSamplesToWavFileFormat(audioClip);
        Stream flac = new MemoryStream();
        SoundTools.Wav2Flac(wav, flac);
        var result = GoogleVoice.GoogleSpeechRequest(flac, sampleRate, language, maxResults);

        GoogleRequest request = new GoogleRequest();
        byte[] audioBytes = SavWav.GetBytes(audioClip);
        string audioEncoded = System.Convert.ToBase64String(audioBytes);
        //request.config.languageCode = "it";
        request.audio.content = audioEncoded;
        //request.audio.uri = "gs://cloud-samples-tests/speech/brooklyn.flac";
        string requestBody = JsonUtility.ToJson(request);

        /*var wav = SoundTools.ConvertSamplesToWavFileFormat(record, sampleRate);
        var flac = new MemoryStream();
        SoundTools.Wav2Flac(wav, flac); */

        HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
        wr.ContentType = "application/json";
        wr.Method = "POST";
        //wr.KeepAlive = true;
        //wr.Credentials = CredentialCache.DefaultCredentials;

        Debug.Log("Request:" + requestBody);

        ASCIIEncoding encoding = new ASCIIEncoding();
        Stream stream = wr.GetRequestStream();
        byte[] buffer = encoding.GetBytes(requestBody);
        stream.Write(buffer, 0, buffer.Length);
        stream.Close();

        //wr.ContentLength = buffer.Length;

        WebResponse wresp = null;
        try
        {
            wresp = wr.GetResponse();
            Stream stream2 = wresp.GetResponseStream();
            StreamReader reader2 = new StreamReader(stream2);

            string responseString = string.Format("{0}", reader2.ReadToEnd());
            Debug.Log("HTTP RESPONSE" + responseString);
            return responseString;

        }
        catch (Exception ex)
        {
            Debug.Log(string.Format("Error uploading file error: {0}", ex));
            if (wresp != null)
            {
                wresp.Close();
                wresp = null;
                return "Error";
            }
        }
        finally
        {
            wr = null;

        }

        return "empty";
    }

    [Serializable]
    class GoogleConfigRequest
    {
        public string encoding;
        public int sampleRateHertz;
        public string languageCode;
        public bool enableWordTimeOffsets;

        public GoogleConfigRequest()
        {
            //encoding = "WAV";
            //sampleRateHertz = 44100;
            languageCode = "en-US";
  /*"maxAlternatives": number,
  "profanityFilter": boolean,
  "speechContexts": */
            enableWordTimeOffsets = false;
        }
    }

    [Serializable]
    struct GoogleAudioRequest
    {
        //public string uri;
        public string content;
    }

    [Serializable]
    class GoogleRequest
    {
        public GoogleConfigRequest config;
        public GoogleAudioRequest audio;

        public GoogleRequest()
        {
            config = new GoogleConfigRequest();
            audio = new GoogleAudioRequest();
        }
    }

    struct ClipData
    {
        public int samples;
    }
}
