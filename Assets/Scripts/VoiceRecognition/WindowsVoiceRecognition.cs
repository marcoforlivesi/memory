﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_STANDALONE_WIN || (UNITY_EDITOR && !UNITY_STANDALONE_LINUX)
using UnityEngine.Windows.Speech;
#endif

public class WindowsVoiceRecognition : BaseVoiceRecognition {
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    private DictationRecognizer dictation;

    // Use this for initialization
    public WindowsVoiceRecognition()
    {
        dictation = new DictationRecognizer();
        dictation.DictationResult += OnDictationResult;
        dictation.DictationHypothesis += OnDictationHypothesis;
        dictation.DictationComplete += OnDictationComplete;
        dictation.DictationError += OnDictationError;
    }

    override public void Start()
    {
        base.Start();
        dictation.Start();
    }

    override public void OnStop()
    {
        dictation.Stop();
    }

    private void OnDictationResult(string text, ConfidenceLevel confidence)
    {
        Debug.Log("result: " + text);
        onTextFound(text);
    }

    private void OnDictationHypothesis(string text)
    {
        if (found)
        {
            return;
        }
        Debug.Log("hypothesis: " + text);
        //onTextFound(text);
    }

    private void OnDictationComplete(DictationCompletionCause cause)
    {
        Debug.Log("completion: " + cause);
    }

    private void OnDictationError(string error, int hresult)
    {
        Debug.Log("error: " + error);
    }
#endif
}
