﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IOSMessageReceiver : MonoBehaviour {
#if UNITY_IOS && !UNITY_EDITOR
	private IOSVoiceRecognition voiceRecognition;

	void Start()
	{
		name = "IOSMessageReceiver";
	}

	public void SetVoiceRecognition(IOSVoiceRecognition voiceRecognition)
	{
		this.voiceRecognition = voiceRecognition;
	}

    public void OnTextFound(string message) {
		if (voiceRecognition == null)
			return;
		voiceRecognition.TextFound (message);
    }

	public void OnError(string error)
	{
		Debug.Log (error);
		GlobalMessage.GetInstance().SetMessage(error);
	}
#endif
}
