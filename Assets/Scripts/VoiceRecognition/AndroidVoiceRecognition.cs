﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidVoiceRecognition : BaseVoiceRecognition {
#if UNITY_ANDROID
    enum SpeechError
    {
        ERROR_NETWORK_TIMEOUT = 1,
        ERROR_NETWORK = 2,
        ERROR_AUDIO = 3,
        ERROR_SERVER = 4,
        ERROR_CLIENT = 5,
        ERROR_SPEECH_TIMEOUT = 6,
        ERROR_NO_MATCH = 7,
        ERROR_RECOGNIZER_BUSY = 8,
        ERROR_INSUFFICIENT_PERMISSIONS = 9
    };


    static private AndroidJavaObject voiceRecognition;
    private AndroidVoiceRecognitionCallback callback;

    static private AndroidVoiceRecognition instance;
    static public AndroidVoiceRecognition GetInstance()
    {
        if (instance == null)
        {
            Init();
        }
        return instance;
    }

    static public AndroidVoiceRecognition Init()
    {
        instance = new AndroidVoiceRecognition();
        return instance;
    }

    private AndroidVoiceRecognition()
    {
        AndroidJavaClass voiceRecognitionClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        voiceRecognition = voiceRecognitionClass.GetStatic<AndroidJavaObject>("currentActivity");

        callback = new AndroidVoiceRecognitionCallback();

        Debug.Log("initVoiceRecognition");
        string language = GameMode.GetLanguage();
        voiceRecognition.Call("init", language, callback);
    }

    public override void Start()
    {
        base.Start();
        if (!callback.isRunning())
        {
            Debug.Log("Start voiceRecognition");
            voiceRecognition.Call("startListening");
        }
        else
        {
            Debug.Log("Voice recognition stil on run");
        }
    }

    public override void OnStop()
    {
        Debug.Log("onStop voiceRecognition");
        if (callback.isRunning())
        {
            UnityMainThreadDispatcher.Instance().Enqueue(() => voiceRecognition.Call("stopListening"));
        }
    }

    public void Speak(string text)
    {
        voiceRecognition.Call("speak", text);
    }

    public void showMessage(string text)
    {
        voiceRecognition.Call("showMessage", text);
    }

    public void SetText(string text)
    {
        isRunning = false;
        if (text.Length > 0 && !found)
        {
            onTextFound(text);
            text = "";
        }
    }

    public void OnError(int error)
    {
        isRunning = false;
        //GlobalMessage.GetInstance().SetMessage(error);
        //showMessage(error);
        Debug.Log("onError: " + error);
        onTextFound("" + error);
    }

    class AndroidVoiceRecognitionCallback : AndroidJavaProxy
    {
        private bool running;

        public AndroidVoiceRecognitionCallback() : base("com.forli.memory.VoiceRecognitionCallback")
        {
            running = false;
        }
        void onReadyForSpeech()
        {
            Debug.Log("onReadyForSpeech");
        }

        void onBeginningOfSpeech()
        {
            Debug.Log("onBeginningOfSpeech");
            running = true;
        }

        void onRmsChanged(float rmsdB)
        {

        }

        void onBufferReceived(byte[] buffer)
        {

        }

        void onEndOfSpeech()
        {
            Debug.Log("onEndOfSpeech");
            running = false;
        }

        void onError(int error)
        {
            Debug.LogError("onError: " + error);
            UnityMainThreadDispatcher.Instance().Enqueue(() => AndroidVoiceRecognition.GetInstance().OnError(error));
        }

        void onResults(string results)
        {
            Debug.Log("result: " + results);
            UnityMainThreadDispatcher.Instance().Enqueue(() => AndroidVoiceRecognition.GetInstance().SetText(results));

            /*string[] arrayText = results.Split('\n');
            for (int i = 0; i < arrayText.Length; i++)
            {
                string text = arrayText[i];
                UnityMainThreadDispatcher.Instance().Enqueue(() => AndroidVoiceRecognition.GetInstance().SetText(text));
            }*/
        }

        void onPartialResults(string results)
        {
            Debug.Log("onPartialResults: " + results);
        }

        void onTextToSpeechError(string error)
        {
            Debug.LogError("onError" + error);
            //UnityMainThreadDispatcher.Instance().Enqueue(() => AndroidVoiceRecognition.GetInstance().OnError(error));
        }

        public bool isRunning()
        {
            return running;
        }
        
    }
#endif
}
