﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class VoiceRecognition
{
    static private VoiceRecognition instance;
    #if UNITY_LINUX || UNITY_EDITOR
    private GoogleVoiceSpeech voiceRecognition;
#elif UNITY_IOS
	private IOSVoiceRecognition voiceRecognition;
#elif UNITY_STANDALONE_WIN
    private WindowsVoiceRecognition voiceRecognition;
#elif UNITY_ANDROID
    private AndroidVoiceRecognition voiceRecognition;
#endif
    public delegate bool OnTextFound(string text);

	// Use this for initialization
	public VoiceRecognition()
    {
#if UNITY_LINUX || UNITY_EDITOR
        voiceRecognition = GoogleVoiceSpeech.GetInstance();
//          voiceRecognition = WatsonVoiceRecognition.GetInstance();
#elif UNITY_IOS
	    voiceRecognition = IOSVoiceRecognition.GetInstance();
#elif UNITY_STANDALONE_WIN
        voiceRecognition = new WindowsVoiceRecognition();
#elif UNITY_ANDROID
        voiceRecognition = AndroidVoiceRecognition.GetInstance();
#endif
    }

    static public VoiceRecognition GetInstance()
    {
        if (instance == null)
        {
            instance = new VoiceRecognition();
        }

        return instance;
    }

    public void setCheckPronunciationFunc(OnTextFound onTextFound)
    {
        if(voiceRecognition != null)
        {
            voiceRecognition.setCheckPronunciationFunc(onTextFound);
        }
    }

    public void Start()
    {
        if(voiceRecognition != null)
        {
            voiceRecognition.Start();
        }
    }

    public void Stop()
    {
        if(voiceRecognition != null)
        {
            voiceRecognition.Stop();
        }
    }

    public bool IsRunning()
    {
        if (voiceRecognition != null)
        {
            return voiceRecognition.IsRunning();
        }

        return false;
    }
}
