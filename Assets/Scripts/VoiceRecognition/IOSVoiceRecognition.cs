﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class IOSVoiceRecognition : BaseVoiceRecognition {
#if UNITY_IOS && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern void _ex_init(string lang);
	[DllImport("__Internal")]
	private static extern void _ex_start();
	[DllImport("__Internal")]
	private static extern void _ex_stop();


	private IOSMessageReceiver iosMessageReceiver;

	static private IOSVoiceRecognition instance;

	static public IOSVoiceRecognition GetInstance()
	{
		if (instance == null)
		{
			instance = new IOSVoiceRecognition();
		}


		return instance;
	}

    static public IOSVoiceRecognition Init()
    {
        instance = new IOSVoiceRecognition();
        return instance;
    }

	private IOSVoiceRecognition()
	{
		string lang = "it-IT";
		_ex_init (lang);

		GameObject receiverGO = new GameObject ();
		this.iosMessageReceiver = receiverGO.AddComponent<IOSMessageReceiver> ();
		iosMessageReceiver.SetVoiceRecognition (this);
	}

	override public void Start()
	{
		base.Start();
		_ex_start ();
	}

	override public void OnStop()
	{
		_ex_stop ();
	}

	public void TextFound(string text)
	{
		instance.onTextFound(text);
        isRunning = false;
	}

	//File.Delete(filePath); //Delete the Temporary Wav file

	// Use this method to call Example.swiftMethod() in Example.swift
	// from other C# classes.
//	public static void CallSwiftMethod(string message) {
//		_ex_callSwiftMethod(message);
		
//	}
#endif
}
