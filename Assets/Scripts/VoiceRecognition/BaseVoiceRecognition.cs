﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseVoiceRecognition
{
    protected bool found;
    protected bool isRunning;
    protected VoiceRecognition.OnTextFound onTextFound;


    public void setCheckPronunciationFunc(VoiceRecognition.OnTextFound onTextFound)
    {
        this.onTextFound = onTextFound;
    }

    virtual public void Start()
    {
        found = false;
        isRunning = true;
    }

    public void Stop()
    {
        found = true;
        if (isRunning)
        {
            OnStop();
        }
    }

    virtual public void OnStop()
    {
        isRunning = false;
    }

    virtual public bool IsRunning()
    {
        return isRunning;
    }
}
