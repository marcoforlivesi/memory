﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System;
using UnityEngine;
using UnityEngine.UI;
using Honeti;

public class Player : BasePlayer
{
    public Text voiceText;

    public float decisionTime = 10.0f;
    protected float remainingTime;

    public void InitVoiceRecognition()
    {
        VoiceRecognition voiceRecognition = VoiceRecognition.GetInstance();
        voiceRecognition.setCheckPronunciationFunc(OnTextFound);
    }

    override public void OnEnter()
    {
        base.OnEnter();
        first = second = null;
        remainingTime = decisionTime;
        InitVoiceRecognition();
    }

    override public void OnTileSelection(MemoryTile tile)
    {
        base.OnTileSelection(tile);
    }

    override protected void Start()
    {
        base.Start();
    }

    override protected void Update()
    {
        base.Update();
        if (!GameMode.GetInstance().IsMyTurn(this))
            return;

        if (remainingTime <= 0)
        {
            if (!VoiceRecognition.GetInstance().IsRunning())
            {
                Cover();
                return;
            }
        }
        else
        {
            remainingTime -= Time.deltaTime;
            text.text = remainingTime.ToString("#.#");
        }
    }

    override protected void AfterFirstSelection()
    {

    }

    override protected void OnSameTile()
    {
        VoiceRecognition voiceRecognition = VoiceRecognition.GetInstance();

        string isVoiceRecognitionActive = PlayerPrefs.GetString("isVoiceRecognitionActive", "true");
        if (isVoiceRecognitionActive != "true")
        {
            Resolved();
            return;
        }

        if (voiceRecognition != null)
        {
            voiceRecognition.Start();
            GlobalMessage.GetInstance().SetMessage(I18N.instance.getValue("^say") + second.name);
        }
        base.OnSameTile();
    }

    override public void OnResolved()
    {
        base.OnResolved();
    }

    override public void OnExit()
    {
        base.OnExit();
    }

    public bool OnTextFound(string text)
    {
        string word = GetWord(text);

        Debug.Log("Player: " + name + " Word: " + word);

        if (voiceText)
        {
            voiceText.text = word;
        }
        GlobalMessage.GetInstance().Hide();
        CancelInvoke("Cover");

        if (word.Equals(second.GetValue(), System.StringComparison.CurrentCultureIgnoreCase))
        {
            Debug.Log("OnTextFound Success!");

            
            Resolved();
            VoiceRecognition voiceRecognition = VoiceRecognition.GetInstance();

            if (voiceRecognition != null)
            {
                voiceRecognition.Stop();
            }

            return true;
        }
        else
        {
            Debug.Log("OnTextFound Fail!");

            Cover();

            return false;
        }
    }

    public string GetWord(string text)
    {
        if (text.Length == 0) return text;

        bool success = text.IndexOf(second.GetValue(), StringComparison.CurrentCultureIgnoreCase) >= 0;
        if (success)
        {
            return second.GetValue();
        }

        /*string[] words = text.Split(' ');

        foreach (string word in words)
        {
            success = word.IndexOf(second.GetValue(), StringComparison.CurrentCultureIgnoreCase) >= 0;
            //if (word.Equals(second.GetValue(), System.StringComparison.CurrentCultureIgnoreCase))

            if (success)
            {
                return second.GetValue();
            }
        }*/

        return text;
    }
}
