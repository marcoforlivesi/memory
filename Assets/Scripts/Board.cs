﻿using DG.Tweening;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour
{
    static private Board instance;

    public MemoryTile prefabTile;
    public int numPair = 6;

    private List<MemoryTile> choices;
    private DeviceOrientation orientation;
    
    private RectTransform rect;
    private Vector2 currentVecSize;

    // Use this for initialization
    void Awake()
    {
        if (instance != null) return;
        instance = this;


        rect = GetComponent<RectTransform>();
        choices = new List<MemoryTile>();
        // trucchetto per far funzionare il layout
        Sillabes.Type[] types = { Sillabes.Type.Two };
        //CreateTiles(6, types);
    }

    static public Board GetInstance()
    {
        return instance;
    }

    public void CreateTiles(int numPair, Sillabes.Type[] types)
    {
        this.numPair = numPair;
        List<string> letters = Sillabes.getList(numPair, types);
        for (int i = 0; i < numPair; i++)
        {
            letters.Add(letters[i]);
        }


        SetCellSize();

        for (int i = 0; i < numPair * 2; i++)
        {
            MemoryTile tile = Instantiate(prefabTile) as MemoryTile;
            tile.transform.SetParent(transform, false);

            int index = Random.Range(0, letters.Count);
            tile.SetText(letters[index]);
            tile.name = letters[index];
            letters.RemoveAt(index);

            choices.Add(tile);
        }
    }

    static private int GetGratestDivisor(int num)
    {
        int divisor = (int) Mathf.Sqrt(num);

        Debug.Log("num: " + num + " divisor: " + divisor);
        while (num % divisor != 0)
        {
            divisor--;
        }

        return divisor;
    }

    public List<MemoryTile> GetChoices()
    {
        return choices;
    }

    public bool isFinished()
    {
        return choices.Count == 0;
    }
    

    public void RemoveChoice(MemoryTile tile)
    {
        choices.Remove(tile);
    }

    void SetCellSize()
    {
        rect.ForceUpdateRectTransforms();

        currentVecSize = rect.rect.size;
        int numTiles = numPair * 2;
        int columns = GetGratestDivisor(numTiles);
        int rows = numTiles / columns;

        int min = Mathf.Min(rows, columns);
        int max = Mathf.Max(rows, columns);

        orientation = Input.deviceOrientation;

        
        GridLayoutGroup gridLayout = gameObject.GetComponent<GridLayoutGroup>();

        orientation = GetOrientation();
        if (orientation == DeviceOrientation.Portrait)
        {
            rows = max;
            columns = min;
        }
        else
        {
            rows = min;
            columns = max;
        }

        gridLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        gridLayout.constraintCount = columns;
        
        float cellSize = Mathf.Min((rect.rect.width - gridLayout.spacing.x * (columns + 1)) / columns, 
            (rect.rect.height - gridLayout.spacing.y * (rows + 1)) / rows);

        gridLayout.cellSize = new Vector2(cellSize, cellSize);
        
    }

    public DeviceOrientation GetOrientation()
    {
        if (Screen.height > Screen.width)
        {
            return DeviceOrientation.Portrait;
        }
        return orientation = DeviceOrientation.LandscapeLeft;
    }

    void Update()
    {
        if (currentVecSize != rect.rect.size)
        {
            SetCellSize();
        }
    }
}
