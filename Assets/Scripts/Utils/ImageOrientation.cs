﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageOrientation : MonoBehaviour {
    public Sprite portrait;
    public Sprite landscape;

    private ScreenOrientation orientation;
    private Image image;


    // Use this for initialization
    void Start () {
        orientation = GetOrientation();
        OnOrientationChange();
	}
	
	// Update is called once per frame
	void Update () {
        ScreenOrientation current = GetOrientation();

        if (orientation != current)
        {
            orientation = current;
            OnOrientationChange();
        }
	}

    public void OnOrientationChange()
    {
        if (orientation == ScreenOrientation.Portrait)
        {
            image.sprite = portrait;
        }
        else
        {
            image.sprite = landscape;
        }
    }

    public ScreenOrientation GetOrientation()
    {
        if (Screen.height > Screen.width)
        {
            return ScreenOrientation.Portrait;
        }
        return orientation = ScreenOrientation.LandscapeLeft;
    }
}
