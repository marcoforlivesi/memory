﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour {
    private static BackButton instance;
    private List<string> levelLoaded;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
        
        levelLoaded = new List<string>();
        levelLoaded.Add(SceneManager.GetActiveScene().name);
        SceneManager.sceneLoaded += OnSceneLoaded;
        DontDestroyOnLoad(gameObject);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnPressBack();
        }
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        string currentScene = scene.name;
        int sceneIndex = levelLoaded.IndexOf(currentScene);
        if (sceneIndex > 0)
        {
            for (int i = sceneIndex; i < levelLoaded.Count; i++)
            {
                levelLoaded.RemoveAt(i);
            }
        }

        levelLoaded.Add(currentScene);

        Debug.Log("OnSceneLoaded Scenes:");
        foreach (string levelName in levelLoaded)
        {
            Debug.Log(levelName);
        }
    }

    public void OnPressBack()
    {
        Debug.Log("OnPressBack Scenes:");
        foreach(string levelName in levelLoaded)
        {
            Debug.Log(levelName);
        }

        if (levelLoaded.Count > 1)
        {
            levelLoaded.RemoveAt(levelLoaded.Count - 1);
            string levelName = levelLoaded[levelLoaded.Count - 1];
            SceneManager.LoadScene(levelName);
        }
        else
        {
            Application.Quit();
        }
    }

}
