﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PreferenceButton : MonoBehaviour {
    public string preferenceKey;
    public string value;
    public Image feedback;
    public bool isDefault;

	// Use this for initialization
	void Start () {
        SetFeedBackEnabled(false);

        string prefValue = PlayerPrefs.GetString(preferenceKey);

        if (IsCurrentChoice(this, prefValue))
        {
            SetFeedBackEnabled(true);
        }
	}

    public void SetPreference()
    {
        SavePreference();
        ClearFeedback(value);
        SetFeedBackEnabled(true);
    }

    public void SavePreference()
    {
        ConfirmPreferences confirmPreferences = FindObjectOfType<ConfirmPreferences>();
        if (confirmPreferences == null) {
            PlayerPrefs.SetString(preferenceKey, value);
        }
        else {
            confirmPreferences.AddPreference(preferenceKey, value);
        }
    }

    public void ClearFeedback(string value)
    {
        if (feedback)
        {
            PreferenceButton[] preferenceButtons = FindObjectsOfType<PreferenceButton>();
            foreach (PreferenceButton preferenceButton in preferenceButtons)
            {
                if (preferenceKey != preferenceButton.preferenceKey)
                    continue;
                
                SetFeedBackEnabled(preferenceButton, false);
            }
        }
    }

    private void SetFeedBackEnabled(bool value)
    {
        SetFeedBackEnabled(this, value);
    }

    private void SetFeedBackEnabled(PreferenceButton button, bool value)
    {
        if (button.feedback)
        {
            button.feedback.enabled = value;
        }
    }

    private bool IsCurrentChoice(PreferenceButton button, string choice)
    {
        if ((choice == null || choice == "") && isDefault)
            return true;
        if (button.value == choice)
            return true;

        return false;
    }
}
