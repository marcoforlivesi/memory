﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImagePreference : MonoBehaviour {
    public string preferenceKey;
    private Image image;

	// Use this for initialization
	void Start () {
        if (!PlayerPrefs.HasKey(preferenceKey))
            return;

        string imagePath = PlayerPrefs.GetString(preferenceKey);
        Sprite sprite = Resources.Load<Sprite>(imagePath);

        image = GetComponent<Image>();
        image.sprite = sprite;
	}
}
