﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sillabes {
    public enum Type { Two, Three, Four };

    static private string[] setTwo = {
        "ge","ce","ra","ar","pu", "qu",
        "io","vo","fi","vi","re", "er","ad","ab",
        "si","is","se","es", "ga","ag","eg","ca",
        "ac", "ec","ba","da", "be","eb","do","od",
        "la","al","le","el", "mi","ni","im","in",
        "pe","ep","ta","at", "to","ot","va","av",
        "ve","ev"
    };

    static private string[] setThree = {
        "che","ghe","sci","sce","tre","ert",
        "pre","erp","dav","vad","far","raf",
        "chi","ghi","lav","val","mar","ram",
        "nap","oan","qua","gua","rat","tar",
        "san","nas","tra","art","vol","lov",
        "zar","raz","sco","osc","rot","tor",
    };

    static private string[] setFour = {
        "bian","naib",
        "darc","crad","forc","crof",
        "guar","raug","lari","ilar",
        "maer","ream","name","opap",
        "quel","lueg","reir","rier",
        "schi","sche","tuor","rout",
        "valz","zlav","zaov","vaoz",
    };


    static public List<string> getList(int num, Type[] types)
    {
        List<string> result = new List<string>();

        //string path = Application.streamingAssetsPath = "2/";
        //System.IO.Directory.GetFiles(path);

        List<string> set = new List<string>();
        if (Contains(types, Type.Two))
        {
            set.AddRange(setTwo);
        }
        if (Contains(types, Type.Three))
        {
            set.AddRange(setThree);
        }
        if (Contains(types, Type.Four))
        {
            set.AddRange(setFour);
        }

        if (set.Count > num)
        {
            while (num > 0)
            {
                int index = Random.Range(0, set.Count);
                result.Add(set[index]);
                set.RemoveAt(index);
                num--;
            }
        }
        else
        {
            result = set;
        }

        return result;
    }

    static private bool Contains<T>(T[] array, T el)
    {
        foreach (T value in array)
        {
            if (value.Equals(el))
            {
                return true;
            }
        }
        return false;
    }
    /*static private string[] setTwo = {
        "bi",
        "ca","co","cu",
        "da", "di",
        "il",
        "le",
        "mo",
        "ni","ne",
        "pa", "po",

        "ra","re","ro",
        "sa","si","se",
        "ta", "ti","to",
        "un",
        "va",
    };

    static private string[] setThree = {
        "abi",
        "bos",
        "che",
        "due",
        "gna",
        "mar",
        "non",
        "sfa",
        "van",
        "riu", "sci",
        "pro", "per",
        "sua", "bam",
        "tes",
    };

    static private string[] setFour = {
        "glia","glie","gran",
        "sten", "suoi"
    };*/
}
