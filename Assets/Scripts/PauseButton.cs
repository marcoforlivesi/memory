﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour {
    public Image image;
    public Sprite play;
    public Sprite pause;


    public void OnPress()
    {
        if (image.sprite == play)
        {
            Time.timeScale = 1;
            image.sprite = pause;
        }
        else
        {
            Time.timeScale = 0;
            image.sprite = play;
        }
    }

}
