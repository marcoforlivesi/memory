﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

using DG.Tweening;

public class PlayerAI : BasePlayer
{
    public float delayDecision = 1.0f;
    private float movementDuration = 0.5f;
    public GameObject hand;
    private MemoryTile choice;

    void Start()
    {
        
    }

    void Update()
    {
        
    }
    

    public override void OnEnter()
    {
        base.OnEnter();
        DoMove();
        hand.SetActive(true);
    }

    public void DoMove()
    {
        if (second != null)
        {
            return;
        }
        Invoke("DoChoice", delayDecision);
    }

    private void DoChoice()
    {
        choice = KnownTile.GetInstance().getChoice(first);

        hand.transform
            .DOMove(choice.transform.position, movementDuration)
            .OnComplete(Show);
    }

    

    private void Show()
    {
        
        choice.Show();
    }

    override protected void AfterFirstSelection()
    {
        base.AfterFirstSelection();
        DoMove();
    }

    protected override void OnSameTile()
    {
        //TextToSpeech.getInstance().Speak(second.GetValue());
        CancelInvoke("Show");
        OnExit();
        Resolved();
    }

    public override void OnExit()
    {
        base.OnExit();
        hand.SetActive(false);
    }
}
