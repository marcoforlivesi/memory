﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Honeti;

public class GameMode : MonoBehaviour
{
    static private GameMode instance;
    
    public GameObject winPrefab;
    public GameObject aiWinPrefab;

    public AudioClip goodSound;
    public AudioClip badSound;

    public BasePlayer[] players;
    private int current;

    private int missing;

    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }

        if (IsSinglePlaerMode())
        {
            KnownTile.Init();
        }

        Debug.Log("GameMode.Start()");
        current = 0;
        players[current].OnEnter();
    }

    static public GameMode GetInstance()
    {
        return instance;
    }


    public void OnTileSelection(MemoryTile tile)
    {
        players[current].OnTileSelection(tile);
    }

    public void ChangePlayer()
    {
        players[current].OnExit();

        current = (current + 1) % players.Length;

        players[current].OnEnter();
    }

    public bool IsMyTurn(BasePlayer player)
    {
        return players[current] == player;
    }

    public bool IsPlayerTurn()
    {
        if (Time.timeScale == 0)
        {
            return false;
        }
        return players[current] is Player && players[current].CanTurn();
    }

    public void OnWin()
    {
        Debug.Log("OnWin");
        int winnerIndex = GetWinnerIndex();

        GameObject result = Instantiate(winPrefab);
        Text winMessage = result.GetComponentInChildren<Text>();
        string playerName = GetPlayerName(winnerIndex);

        winMessage.text = I18N.instance.getValue("^winnerMsg").Replace("e'", "è")
            + ":\n" + playerName;

        Destroy(Board.GetInstance().gameObject);

        GameObject background = GameObject.Find("Scene");
        LevelDirector levelDirector = LevelDirector.GetInstance();
        if (levelDirector != null && winnerIndex == 0)
        {
            LoadLevel loadLevel = result.GetComponent<LoadLevel>();
            loadLevel.levelName = SceneManager.GetActiveScene().name;
        }

        result.transform.SetParent(background.transform, false);
    }

    string GetPlayerName(int index)
    {
        if (players[index] is PlayerAI)
        {
            return I18N.instance.getValue("^robot");
        }

        return I18N.instance.getValue("^player") + " " + (index + 1);
    }

    private int GetWinnerIndex()
    {
        int maxScore = 0, maxScoreIndex = 0;
        for (int i = 0; i < players.Length; i++) 
        {
            int score = players[i].GetScore();
            if (score > maxScore)
            {
                maxScore = score;
                maxScoreIndex = i;
            }
        }
        return maxScoreIndex;
    }

    public bool IsSinglePlaerMode()
    {
        foreach (BasePlayer player in players)
        {
            if (player is PlayerAI)
            {
                return true;
            }
        }

        return false;
    }

    static public string GetLanguage()
    {
        const string GAME_LANG = "game_language";

        string res = "en-US";
        if (!PlayerPrefs.HasKey(GAME_LANG))
        {
            SystemLanguage lang = Application.systemLanguage;
            switch (lang)
            {
                case SystemLanguage.Afrikaans: res = "AF"; break;
                case SystemLanguage.Arabic: res = "AR"; break;
                case SystemLanguage.Basque: res = "EU"; break;
                case SystemLanguage.Belarusian: res = "BY"; break;
                case SystemLanguage.Bulgarian: res = "BG"; break;
                case SystemLanguage.Catalan: res = "CA"; break;
                case SystemLanguage.Chinese: res = "ZH"; break;
                case SystemLanguage.Czech: res = "CS"; break;
                case SystemLanguage.Danish: res = "DA"; break;
                case SystemLanguage.Dutch: res = "NL"; break;
                case SystemLanguage.English: res = "EN"; break;
                case SystemLanguage.Estonian: res = "ET"; break;
                case SystemLanguage.Faroese: res = "FO"; break;
                case SystemLanguage.Finnish: res = "FI"; break;
                case SystemLanguage.French: res = "FR"; break;
                case SystemLanguage.German: res = "DE"; break;
                case SystemLanguage.Greek: res = "EL"; break;
                case SystemLanguage.Hebrew: res = "IW"; break;
                case SystemLanguage.Icelandic: res = "IS"; break;
                case SystemLanguage.Indonesian: res = "IN"; break;
                case SystemLanguage.Italian: res = "it-IT"; break;
                case SystemLanguage.Japanese: res = "JA"; break;
                case SystemLanguage.Korean: res = "KO"; break;
                case SystemLanguage.Latvian: res = "LV"; break;
                case SystemLanguage.Lithuanian: res = "LT"; break;
                case SystemLanguage.Norwegian: res = "NO"; break;
                case SystemLanguage.Polish: res = "PL"; break;
                case SystemLanguage.Portuguese: res = "PT"; break;
                case SystemLanguage.Romanian: res = "RO"; break;
                case SystemLanguage.Russian: res = "RU"; break;
                case SystemLanguage.SerboCroatian: res = "SH"; break;
                case SystemLanguage.Slovak: res = "SK"; break;
                case SystemLanguage.Slovenian: res = "SL"; break;
                case SystemLanguage.Spanish: res = "ES"; break;
                case SystemLanguage.Swedish: res = "SV"; break;
                case SystemLanguage.Thai: res = "TH"; break;
                case SystemLanguage.Turkish: res = "TR"; break;
                case SystemLanguage.Ukrainian: res = "UK"; break;
                case SystemLanguage.Unknown: res = "EN"; break;
                case SystemLanguage.Vietnamese: res = "VI"; break;
            }
        }
        else
        {
            res = PlayerPrefs.GetString(GAME_LANG);
        }
        if (res == "EN")
        {
            res = "en-US";
        }
        else if (res == "IT")
        {
            res = "it-IT";
        }

        Debug.Log("Language: " + res);

        return res;
    }
}
