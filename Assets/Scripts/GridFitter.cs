﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridFitter : MonoBehaviour {
    public int numTiles = 4;


    private List<MemoryTile> choices;
    private DeviceOrientation orientation;


    private RectTransform rect;
    private Vector2 currentVecSize;

    void Start()
    {
        rect = GetComponent<RectTransform>();
        SetCellSize();
    }

    void Update()
    {
        if (currentVecSize != rect.rect.size)
        {
            SetCellSize();
        }
    }

    void SetCellSize()
    {
        rect.ForceUpdateRectTransforms();

        currentVecSize = rect.rect.size;
        int columns = GetGratestDivisor(numTiles);
        int rows = numTiles / columns;

        int min = Mathf.Min(rows, columns);
        int max = Mathf.Max(rows, columns);

        orientation = Input.deviceOrientation;


        GridLayoutGroup gridLayout = gameObject.GetComponent<GridLayoutGroup>();

        orientation = GetOrientation();
        if (orientation == DeviceOrientation.Portrait)
        {
            rows = max;
            columns = min;
        }
        else
        {
            rows = min;
            columns = max;
        }

        gridLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        gridLayout.constraintCount = columns;

        float cellSize = Mathf.Min((rect.rect.width - gridLayout.spacing.x * (columns + 1)) / columns,
            (rect.rect.height - gridLayout.spacing.y * (rows + 1)) / rows);

        gridLayout.cellSize = new Vector2(cellSize, cellSize);

    }

    public DeviceOrientation GetOrientation()
    {
        if (Screen.height > Screen.width)
        {
            return DeviceOrientation.Portrait;
        }
        return orientation = DeviceOrientation.LandscapeLeft;
    }

    public DeviceOrientation getOrientation()
    {
        if (Screen.height > Screen.width)
        {
            return DeviceOrientation.Portrait;
        }
        return orientation = DeviceOrientation.LandscapeLeft;
    }

    

    static private int GetGratestDivisor(int num)
    {
        int divisor = (int)Mathf.Sqrt(num);
        while (num % divisor != 0)
        {
            divisor--;
        }
        return divisor;
    }
}
