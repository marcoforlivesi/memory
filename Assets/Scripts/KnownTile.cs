﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnownTile {
	private static KnownTile instance;

    private List<MemoryTile> knownTiles;

    public KnownTile()
    {
        knownTiles = new List<MemoryTile>();
    }

    static public void Init()
    {
        instance = new KnownTile();
    }

    static public KnownTile GetInstance()
	{
		if (instance == null)
		{
            Init();
		}

		return instance;
	}

	public void addShowed(MemoryTile tile)
    {
        if (tile == null) return;
		if (knownTiles.Contains(tile))
		{
			return;
		}

        knownTiles.Add(tile);
    }

    public void removeShowed(MemoryTile tile)
    {
        knownTiles.Remove(tile);
    }

    public List<MemoryTile> getKnownTile()
    {
        return knownTiles;
    }

	public MemoryTile getChoice(MemoryTile first)
	{
        List<MemoryTile> choices = knownTiles;
        knownTiles.Remove(first);

        MemoryTile choice;
        if (first != null)
        {
            choice = getKnownTile(first);
            if (choice != null)
            {
                return choice;
            }

            choices = Board.GetInstance().GetChoices();
            
        }

        if (knownTiles.Count == 0 || (knownTiles.Count == 1 && first != null && knownTiles[0].name != first.name))
        {
            choices = Board.GetInstance().GetChoices();
        }

        choice = getRandomChoice(choices, first);
		return choice;
	}

    private MemoryTile getRandomChoice(List<MemoryTile> choices, MemoryTile first)
    {
        choices.Remove(first);
        int index = Random.Range(0, choices.Count);
        if (first != null)
        {
            choices.Add(first);
        }

        return choices[index];
    }

    public MemoryTile getKnownTile(MemoryTile first)
    {
        foreach (MemoryTile tile in knownTiles)
        {
            if (tile != first && tile.name == first.name)
                return tile;
        }

        return null;
    }
}
