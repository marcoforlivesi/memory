﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class BasePlayer : MonoBehaviour
{
    protected MemoryTile first;
    protected MemoryTile second;

    public float delayOnError = 0.5f;
    public float delayOnSame = 5.0f;

    public Text text;
    public Image back;
    public AudioSource audio;

    public Score score;


    virtual public void OnEnter()
    {
        first = second = null;
        back.color = new Color(1, 0.45f, 0, 1);
    }

    virtual public void OnTileSelection(MemoryTile tile)
    {
        Debug.Log("OnTileSelection: " + tile +
            " first: " + first + " second: " + second);

        KnownTile.GetInstance().addShowed(tile);


        if (first == tile || second == tile) return;
        if (first == null)
        {
            first = tile;
            AfterFirstSelection();
            return;
        }

        second = tile;

        if (first.isSame(second))
        {
            OnSameTile();
        }
        else
        {
            Invoke("Cover", delayOnError);
        }
    }

    protected virtual void Start()
    {
    }

    protected virtual void Update()
    {
        
    }

    virtual protected void AfterFirstSelection()
    {

    }

    virtual protected void OnSameTile()
    {
        Invoke("Cover", delayOnSame);
    }

    public void Cover()
    {
        Debug.Log("Cover " + first + " " + second);
        if (first != null) first.Cover();
        if (second != null) second.Cover();

        OnCovered();
    }

    public void Resolved()
    {
        audio.clip = GameMode.GetInstance().goodSound;
        audio.Play();

        first.Resolved();
        second.Resolved();

        KnownTile.GetInstance().removeShowed(first);
        KnownTile.GetInstance().removeShowed(second);

        OnResolved();
    }

    virtual public void OnCovered()
    {
        audio.clip = GameMode.GetInstance().badSound;
        if (!audio.isPlaying)
        {
            audio.Play();
        }

        VoiceRecognition voiceRecognition = VoiceRecognition.GetInstance();
        if (voiceRecognition != null)
        {
            voiceRecognition.Stop();
        }
        GameMode.GetInstance().ChangePlayer();
    }

    virtual public void OnResolved()
    {
        score.addPoint();        

        if (Board.GetInstance().GetChoices().Count == 0)
        {
            OnWin();
            return;
        }
        OnEnter();
    }

    virtual public void OnExit()
    {
        CancelInvoke("Cover");
        back.color = Color.white;
    }

    public bool CanTurn()
    {
        return second == null;
    }

    public int GetScore()
    {
        return score.GetPoints();
    }

    virtual protected void OnWin()
    {
        GameMode.GetInstance().OnWin();
        gameObject.SetActive(false);
    }
}
