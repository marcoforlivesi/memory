﻿#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace UnitySwift {
    public static class PostProcessor {
        [PostProcessBuild]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string buildPath) {

			if (buildTarget == BuildTarget.iOS) {

				// Get plist
				string plistPath = buildPath + "/Info.plist";
				PlistDocument plist = new PlistDocument();
				plist.ReadFromString(File.ReadAllText(plistPath));

				// Get root
				PlistElementDict rootDict = plist.root;

				// Change value of CFBundleVersion in Xcode plist
				var buildKey = "NSSpeechRecognitionUsageDescription";
				rootDict.SetString(buildKey,"Sillabes recognition");

				// Write to file
				File.WriteAllText(plistPath, plist.WriteToString());

                // So PBXProject.GetPBXProjectPath returns wrong path, we need to construct path by ourselves instead
                // var projPath = PBXProject.GetPBXProjectPath(buildPath);
                var projPath = buildPath + "/Unity-iPhone.xcodeproj/project.pbxproj";
				var project = new PBXProject();
				project.ReadFromFile(projPath);

				var targetGUID = project.TargetGuidByName(PBXProject.GetUnityTargetName());

				project.AddFrameworkToProject(targetGUID, "Speech.framework", false);

				project.WriteToFile(projPath);
            }
        }
    }
}
#endif