#import <UIKit/UIKit.h>

@class AVAudioEngine;
@class SFSpeechRecognizer;
@class SFSpeechAudioBufferRecognitionRequest;
@class SFSpeechRecognitionTask;

@class RecorderView;

@interface SpeechRecognition : NSObject

 @property (copy, nonatomic) void (^onTextFound)(NSString *text);

@property (nonatomic,strong) AVAudioEngine *audioEngine;
@property (nonatomic,strong) SFSpeechRecognizer *speechRecognizer;
@property (nonatomic,strong) __block SFSpeechAudioBufferRecognitionRequest *request;
@property (nonatomic,strong) SFSpeechRecognitionTask *recognitionTask;

-(void) initSpeech:(NSString*) lang;
-(void) startRecording;
-(void) stopRecording;

@end
