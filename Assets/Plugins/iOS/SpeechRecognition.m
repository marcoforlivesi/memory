
#import "SpeechRecognition.h"
#import <Speech/Speech.h>

#define autoStopRecordSec 15    //unit: 0.1(sec)

@interface SpeechRecognition ()  <SFSpeechRecognizerDelegate,SFSpeechRecognitionTaskDelegate>

@end

@implementation SpeechRecognition {
    
}

- (void) initSpeech: (NSString*) lang {
    _audioEngine = [[AVAudioEngine alloc] init];

    NSLocale *locale = [NSLocale localeWithLocaleIdentifier: lang];
    _speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale: locale];
    [_speechRecognizer setDelegate:self];
    _request = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    
    if (![_speechRecognizer isAvailable])
    {
        NSString *error = [NSString stringWithFormat: @"Language %@ not available", lang];
        NSLog(@"%@", error);
        UnitySendMessage("IOSMessageReceiver", "OnError", [error UTF8String]);
    }
    
    AVAudioInputNode *node =[_audioEngine inputNode];
    AVAudioFormat *recordingFormat = [node outputFormatForBus:0];
    
    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
        NSLog(@"SFSpeechRecognizer Status:");
        NSString *error;
        
        switch (status) {
            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
                NSLog(@"\tDon't know yet");
                error = @"Don't know yet";
                UnitySendMessage("IOSMessageReceiver", "OnError", [error UTF8String]);
                break;
            case SFSpeechRecognizerAuthorizationStatusDenied:
                NSLog(@"\tUser said no");
                error = @"User said no";
                UnitySendMessage("IOSMessageReceiver", "OnError", [error UTF8String]);
                break;
            case SFSpeechRecognizerAuthorizationStatusRestricted:
                NSLog(@"\tDevice isn't permitted");
                error = @"Device isn't permitted";
                UnitySendMessage("IOSMessageReceiver", "OnError", [error UTF8String]);
                break;
            case SFSpeechRecognizerAuthorizationStatusAuthorized:
                NSLog(@"\tGood to go");
                break;
        }
    }];
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        NSLog(@"Mircophone:");
        if (granted) {
            NSLog(@"\tGood to go");
        }
        else {
            NSLog(@"\tDevice isn't permitted");
            NSString *error = @"Device isn't permitted";
            UnitySendMessage("IOSMessageReceiver", "OnError", [error UTF8String]);
        }
    }];
    
    
    // call frequency 0.1 sec
    [node installTapOnBus:0 bufferSize:1024 format:recordingFormat block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        
        [_request appendAudioPCMBuffer:buffer];
        
    }];
}

-(void) startRecording {
    
    if(![_audioEngine isRunning]) {
        [_audioEngine prepare];
        
        NSError *error;
        if([_audioEngine startAndReturnError:(&error)]) {
            _recognitionTask = [_speechRecognizer recognitionTaskWithRequest:_request delegate:self];
        }
        else {
            NSLog(@"Error %@",error);
            UnitySendMessage("IOSMessageReceiver", "OnError", [[error localizedDescription] UTF8String]);
        }
    }
}

-(void) stopRecording {
    
    if([_audioEngine isRunning]) {
        [_audioEngine stop];
        [_request endAudio];
        [_recognitionTask finish];
        
        //SFSpeechAudioBufferRecognitionRequest cannot be re-used
        _request = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    }
}

#pragma mark - SFSpeechRecognition
// Called when the availability of the given recognizer changes
- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available {
    
    NSLog(@"speechRecognizer");
    //    EX: internet stage change
}

// Called when the task first detects speech in the source audio
- (void)speechRecognitionDidDetectSpeech:(SFSpeechRecognitionTask *)task {
    
    NSLog(@"speechRecognitionDidDetectSpeech");
}

// Called for all recognitions, including non-final hypothesis
- (void)speechRecognitionTask:(SFSpeechRecognitionTask *)task didHypothesizeTranscription:(SFTranscription *)transcription {
    
    //    NSLog(@"speechRecognitionTask");
}


// Called only for final recognitions of utterances. No more about the utterance will be reported
- (void)speechRecognitionTask:(SFSpeechRecognitionTask *)task didFinishRecognition:(SFSpeechRecognitionResult *)recognitionResult{
    
    NSLog(@"speechRecognitionTask");
    
    NSString *translatedString = [[recognitionResult bestTranscription] formattedString];
    NSArray<SFTranscription *> *transcriptions = [recognitionResult transcriptions];
    
    NSUInteger count = [transcriptions count];
    NSString* transcriptionStrings = @"";
    for (NSUInteger i = 0; i < count; i++) {
        SFTranscription* transcription = [transcriptions objectAtIndex: i];
        transcriptionStrings = [transcriptionStrings stringByAppendingString: [transcription formattedString]];
        transcriptionStrings = [transcriptionStrings stringByAppendingString: @"\n"];
    }
    
    NSLog(@"transcriptions: %@", transcriptionStrings);
    NSLog(@"best: %@", translatedString);
    //[self onTextFound: translatedString];
    UnitySendMessage("IOSMessageReceiver", "OnTextFound", [translatedString UTF8String]);
}

// Called when the task is no longer accepting new audio but may be finishing final processing
- (void)speechRecognitionTaskFinishedReadingAudio:(SFSpeechRecognitionTask *)task {
    
    NSLog(@"speechRecognitionTaskFinishedReadingAudio");
}

// Called when the task has been cancelled, either by client app, the user, or the system
- (void)speechRecognitionTaskWasCancelled:(SFSpeechRecognitionTask *)task {
    
    NSString *error = @"speechRecognitionTaskWasCancelled";
    NSLog(@"speechRecognitionTaskWasCancelled");
    UnitySendMessage("IOSMessageReceiver", "OnError", [error UTF8String]);
}

// Called when recognition of all requested utterances is finished.
// If successfully is false, the error property of the task will contain error information
- (void)speechRecognitionTask:(SFSpeechRecognitionTask *)task didFinishSuccessfully:(BOOL)successfully {
    
    NSLog(@"speechRecognitionTask");
}

@end

