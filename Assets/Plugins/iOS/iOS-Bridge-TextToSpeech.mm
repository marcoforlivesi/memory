
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

static AVSpeechSynthesizer *synthesizer = NULL;
static AVSpeechUtterance *speechutt = NULL;

extern "C" {
    void _ex_speak_init(const char* lang) {
        if (synthesizer == nil)
        {
            synthesizer = [[AVSpeechSynthesizer alloc]init];
            
            [speechutt setRate:0.3f];
            NSString *langString = [NSString stringWithUTF8String: lang];
            speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage: langString];
            //speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-us"];
        }
    }
    
    void _ex_speak(const char *message) {
        NSString *str = [NSString stringWithUTF8String: message];
        speechutt = [AVSpeechUtterance speechUtteranceWithString:str];
        [synthesizer speakUtterance:speechutt];
    }
}

