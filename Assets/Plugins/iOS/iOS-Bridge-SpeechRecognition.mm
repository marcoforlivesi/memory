
#import <AVFoundation/AVFoundation.h>
#import "SpeechRecognition.h"

static SpeechRecognition *speechRecognition = NULL;

extern "C" {
    void _ex_init(const char* lang) {
        if (speechRecognition == nil)
        {
            speechRecognition = [[SpeechRecognition alloc]init];
        }
        [speechRecognition initSpeech: [NSString stringWithUTF8String: lang]];
    }
    
    void _ex_start() {
        [speechRecognition startRecording];
    }
    
    void _ex_stop() {
        [speechRecognition stopRecording];
    }
}

