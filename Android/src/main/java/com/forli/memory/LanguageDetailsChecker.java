package com.forli.memory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;

import java.util.List;

/**
 * Created by Marco on 17/03/2018.
 */

class LanguageDetailsChecker extends BroadcastReceiver {
    private List<String> supportedLanguages;
    private String LOG_TAG = "VoiceRecognitionActivity";

    private String languagePreference;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Bundle results = getResultExtras(true);
        if (results.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE))
        {
            languagePreference = results.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE);

        }
        if (results.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES))
        {
            supportedLanguages = results.getStringArrayList(
                            RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES);

        }
        Log.d(LOG_TAG, "EXTRA_LANGUAGE_PREFERENCE: " + languagePreference);
        Log.d(LOG_TAG, "EXTRA_SUPPORTED_LANGUAGES: " + supportedLanguages);
    }
}
