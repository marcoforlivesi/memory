//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.forli.memory;

public interface VoiceRecognitionCallback {
    void onReadyForSpeech();

    void onBeginningOfSpeech();

    void onRmsChanged(float var1);

    void onBufferReceived(byte[] var1);

    void onEndOfSpeech();

    void onError(int errorCode);

    void onResults(String var1);

    void onPartialResults(String var1);

    void onTextToSpeechError(String var1);
}

