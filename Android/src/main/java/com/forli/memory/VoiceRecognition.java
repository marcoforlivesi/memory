//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.forli.memory;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;
import com.unity3d.player.UnityPlayerActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

public class VoiceRecognition extends UnityPlayerActivity implements RecognitionListener, OnInitListener {
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 100;
    private static VoiceRecognition instance;
    private SpeechRecognizer speech = null;
    private Intent recognizerIntent;
    private String LOG_TAG = "VoiceRecognitionActivity";
    private TextToSpeech textToSpeech;
    private String language;
    private VoiceRecognitionCallback callback;

    public VoiceRecognition() {
        if (instance == null)
        {
            instance = this;
        }
    }

    public static VoiceRecognition getInstance()
    {
        return instance;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void init(final String language, VoiceRecognitionCallback voiceCallback) {
        this.callback = voiceCallback;
        this.language = language;

        this.textToSpeech = new TextToSpeech(this, this);
    }

    @Override
    public void onInit(int status) {
        if (speech != null)
        {
            speech.destroy();
        }

        this.speech = SpeechRecognizer.createSpeechRecognizer(this);
        Log.i(this.LOG_TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this));

        this.speech.setRecognitionListener(this);
        this.recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        this.recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        this.recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, language);
        this.recognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, language);
        this.recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        this.recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);

        Intent detailsIntent =  new Intent(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS);
        sendOrderedBroadcast(
                detailsIntent, null, new LanguageDetailsChecker(), null, Activity.RESULT_OK, null, null);


        if (status == 0) {
            int result = VoiceRecognition.this.textToSpeech.setLanguage(new Locale(language));
            if (result == -1 || result == -2) {
                Log.e("error", "This Language is not supported");
                VoiceRecognition.this.callback.onTextToSpeechError("This Language is not supported");
            }
        } else {
            Log.e("error", "Initilization Failed!");
            VoiceRecognition.this.callback.onTextToSpeechError("Initilization Failed!");
        }

    }

    public void startListening()
    {
        Log.d(LOG_TAG, "VoiceRecognition startListening");

        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (ContextCompat.checkSelfPermission(instance,
                        Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED )
                {
                    Log.d(LOG_TAG, "NotGranted");
                    ActivityCompat.requestPermissions(instance, new String[]{Manifest.permission.RECORD_AUDIO}, VOICE_RECOGNITION_REQUEST_CODE);
                }
                else {
                    Log.d(LOG_TAG, "Granted");
                    instance.speech.startListening(instance.recognizerIntent);
                }
            }
        });

    }

    public void stopListening()
    {
        Log.d(LOG_TAG, "VoiceRecognition stopListening");
        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(LOG_TAG, "stopCall");
                VoiceRecognition.getInstance().speech.stopListening();
            }
        });
    }

    public void cancel()
    {
        Log.d(LOG_TAG, "VoiceRecognition cancel");
        Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(LOG_TAG, "cancelCall");
                VoiceRecognition.getInstance().speech.cancel();
            }
        });
    }

    public void speak(String text) {
        this.textToSpeech.speak(text, 0, (HashMap)null);
    }

    public  void showMessage(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(LOG_TAG, "VoiceRecognition onRequestPermissionsResult");
        if(requestCode == VOICE_RECOGNITION_REQUEST_CODE)
        {
            if (grantResults.length > 0 && grantResults[0] == 0)
            {
                Log.d(LOG_TAG, "has Permission");
                this.speech.startListening(this.recognizerIntent);
                return;
            }
        }
        Log.e("error", "onRequestPermissionsResult Failed!");
        Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
        this.callback.onError(SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS);
    }

    public void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
        if (this.speech != null) {
            this.speech.destroy();
        }

    }

    public void onBeginningOfSpeech() {
        Log.i(this.LOG_TAG, "onBeginningOfSpeech");
        this.callback.onBeginningOfSpeech();
    }

    public void onBufferReceived(byte[] buffer) {
        this.callback.onBufferReceived(buffer);
    }

    public void onEndOfSpeech() {
        Log.i(this.LOG_TAG, "onEndOfSpeech");
        this.callback.onEndOfSpeech();
    }

    public void onError(int errorCode) {
        String errorMessage = getErrorText(errorCode);
        Log.d(this.LOG_TAG, "FAILED " + errorMessage);
        this.callback.onError(errorCode);
    }

    public void onEvent(int arg0, Bundle arg1) {
    }

    public void onReadyForSpeech(Bundle arg0) {
        Log.i(this.LOG_TAG, "onReadyForSpeech");
        this.callback.onReadyForSpeech();
    }

    public void onPartialResults(Bundle results) {
        Log.i(this.LOG_TAG, "onPartialResults");
        ArrayList<String> matches = results.getStringArrayList("results_recognition");
        String text = "";

        String result;
        for(Iterator var4 = matches.iterator(); var4.hasNext(); text = text + result + "\n") {
            result = (String)var4.next();
        }

        this.callback.onPartialResults(text);
    }

    public void onResults(Bundle results) {
        Log.i(this.LOG_TAG, "onResults");
        ArrayList<String> matches = results.getStringArrayList("results_recognition");
        String text = "";

        String result;
        for(Iterator var4 = matches.iterator(); var4.hasNext(); text = text + result + "\n") {
            result = (String)var4.next();
        }

        this.callback.onResults(text);
    }

    public void onRmsChanged(float rmsdB) {
        this.callback.onRmsChanged(rmsdB);
    }

    public static String getErrorText(int errorCode) {
        String message;
        switch(errorCode) {
        case 1:
            message = "Network timeout";
            break;
        case 2:
            message = "Network error";
            break;
        case 3:
            message = "Audio recording error";
            break;
        case 4:
            message = "error from server";
            break;
        case 5:
            message = "Client side error";
            break;
        case 6:
            message = "No speech input";
            break;
        case 7:
            message = "No match";
            break;
        case 8:
            message = "RecognitionService busy";
            break;
        case 9:
            message = "Insufficient permissions";
            break;
        default:
            message = "Didn't understand, please try again.";
        }

        return message;
    }
}

